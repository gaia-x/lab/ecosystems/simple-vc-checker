# VC Checker

UI and API for simple validation, not Gaia-X specific

This tool will check:

- Is that a valid JSON input
- Are the context valids (can they be fetched + valid format)
- Is that a valid JSON-LD file
- Is that a VC
- Is that a signed VC
- Is the verificationMethod supported
- Is the public key material reachable (local uniresolver with https, did:key and did:web support. To be extended later)
- Is the signature valid (re-normalise + hash)

## Installation
```
yarn install
```

Start the server:
```
yarn start
```

Development:
```
yarn dev
```

## API
POST /api/validation

## License
EPL-2.0
https://www.eclipse.org/legal/epl-2.0/



