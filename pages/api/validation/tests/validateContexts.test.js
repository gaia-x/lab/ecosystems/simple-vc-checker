import validateContexts from "../validateContexts";
import { describe, it, expect, vi, beforeEach } from "vitest";

vi.mock("../validateJSON.js");

global.fetch = vi.fn().mockResolvedValue({
  json: vi.fn().mockResolvedValue({ foo: "bar" }),
});

const document = { "@context": ["https://www.w3.org/2018/credentials/v1"] };

describe("validateContexts", () => {
  it("should return false if the document has no @context", () => {
    const document = { foo: "bar" };
    expect(validateContexts(document)).toBe(false);
  });

  it("should fetch the context with its url", () => {
    validateContexts(document);
    expect(fetch).toHaveBeenCalledWith(document["@context"][0]);
  });

  it("should return true if the context is a valid JSON", async () => {
    const validateJSON = await import("../validateJSON.js");
    validateJSON.default = vi.fn().mockReturnValueOnce(true);
    expect(validateContexts(document)).toBeTruthy();
    expect(validateJSON.default).toHaveBeenCalled();
  });

  it("should return false if the context is an invalid JSON", async () => {
    const validateJSON = await import("../validateJSON.js");
    validateJSON.default = vi.fn().mockReturnValueOnce(false);
    expect(validateContexts(document)).toBeFalsy();
  });
});
