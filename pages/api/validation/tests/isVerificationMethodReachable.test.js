import { isVerificationMethodReachable } from "../isVerificationMethodReachable.js";
import { describe, it, expect } from "vitest";

describe("isVerificationMethodReachable", () => {
  it("should return true if all verification methods are reachable", async () => {
    const document = {
      proof: [
        { verificationMethod: "https://example.com/method1" },
        { verificationMethod: "https://example.com/method2" },
      ],
    };

    const result = await isVerificationMethodReachable(document);
    expect(result).toBeTruthy();
  });

  it("should return false if any verification method is not reachable", async () => {
    const document = {
      proof: [
        { verificationMethod: "xxxxxxxx" },
        { verificationMethod: "https://example.com/method2" },
      ],
    };

    const result = await isVerificationMethodReachable(document);
    expect(result).toBeFalsy();
  });
});
