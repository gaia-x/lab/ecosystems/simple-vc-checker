import { isVerificationTypeSuppported } from "../isVerificationTypeSuppported.js";
import { describe, it, expect } from "vitest";

describe("isVerificationTypeSuppported", () => {
  it("should return true if all proof types are supported", () => {
    const document = {
      proof: [
        { type: "Ed25519Signature2018" },
        { type: "Ed25519Signature2020" },
      ],
    };
    const result = isVerificationTypeSuppported(document);
    expect(result).toBeTruthy();
  });

  it("should return false if any proof type is not supported", () => {
    const document = {
      proof: [
        { type: "Ed25519Signature2018" },
        { type: "UnsupportedSignature" },
      ],
    };
    const result = isVerificationTypeSuppported(document);
    expect(result).toBeFalsy();
  });
});
