import { describe, it, expect, vi } from "vitest";
import vc from "../../../../data/vc01.json";
import validateContexts from "../validateContexts";

describe("validateVerifiableCredential", () => {
  it("should return a report with all needed properties", async () => {
    const properties = [
      "isValidJson",
      "areContextsValid",
      "isVerifiableCredential",
      "isSignedVerifiableCredential",
      "isSignatureTypeSupported",
      "isVerificationMethodReachable",
      "isValidSignature",
    ];

    const report = await validateVerifiableCredential(vc);

    expect(Object.keys(report)).toEqual(properties);
  });

  it("should call all validating functions", async () => {
    const validateContextsSpy = vi.spyOn(validateContexts);

    await validateVerifiableCredential(vc);

    expect(validateContextsSpy).toHaveBeenCalled();
  });
});
