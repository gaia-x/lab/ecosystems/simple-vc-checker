import { loadVerificationMethod } from "../loadVerificationMethod.js";
import { describe, it, expect, vi, beforeEach } from "vitest";

global.fetch = vi.fn().mockResolvedValue({
  json: vi.fn().mockResolvedValue({ foo: "bar" }),
});

describe("loadVerificationMethod", () => {
  it("should call fetch and returns a json if url is an https one", async () => {
    const didDocument = await loadVerificationMethod("https://example.com");
    expect(fetch).toHaveBeenCalledWith("https://example.com");
    expect(didDocument).toEqual({ foo: "bar" });
  });

  it("should returns a DID document when given a valid DID key", async () => {
    const didDocument = await loadVerificationMethod("did:key:123");
    expect(didDocument).toBeDefined();
  });

  it("should returns false when an error occurs or when method is not supported", async () => {
    const didDocument = await loadVerificationMethod("invalid input");
    expect(didDocument).toBeFalsy();
  });
});
