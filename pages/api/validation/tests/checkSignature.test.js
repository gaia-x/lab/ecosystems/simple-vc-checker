import checkSignature from "../checkSignature.js";
import { loadVerificationMethod } from "../loadVerificationMethod.js";
import { describe, it, expect, vi } from "vitest";

vi.mock("../loadVerificationMethod", () => {
  return {
    loadVerificationMethod: vi.fn().mockResolvedValue({}),
  };
});

describe("checkSignature", () => {
  const document = {
    "@context": "https://w3id.org/security/v2",
    id: "did:example:123456789abcdefghi",
    created: "2022-01-01T01:00:00Z",
    proof: [
      {
        type: "Ed25519Signature2018",
        created: "2022-01-01T01:00:00Z",
        proofPurpose: "assertionMethod",
        verificationMethod: "did:example:123456789abcdefghi#key-1",
        jws: "eyJhbGciOiJFZERTQSIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..WQsfrZd4Oi4_zR_QkAo6P0U6S18U6OyB3TfT7VjK0Mpyf7V0LzLRuO7H9iQc0X0I_g_1e_V7fjn72lzV7nHAg",
      },
    ],
  };

  it("should return true if the signature is valid", async () => {
    const verificationMethod = {
      "@context": "https://w3id.org/security/v2",
      type: "Ed25519VerificationKey2018",
      id: "https://mercury-credentials-public-tb0172-prod.s3.us-east-1.amazonaws.com/publicKey.json",
      controller:
        "https://mercury-credentials-public-tb0172-prod.s3.us-east-1.amazonaws.com/controller.json",
      publicKeyBase58: "7y5LRj1nfKpC3Bg5fjqJRYEnqkoVDg4DezQpX6zhRasT",
    };
    loadVerificationMethod.mockResolvedValue(verificationMethod);
    const result = await checkSignature(document);
    expect(result).toBe(true);
    expect(loadVerificationMethod).toHaveBeenCalledWith(
      "did:example:123456789abcdefghi#key-1"
    );
  });

  it("should return false if the signature is invalid", async () => {
    loadVerificationMethod.mockResolvedValue(false);
    const result = await checkSignature(document);
    expect(result).toBe(false);
    expect(loadVerificationMethod).toHaveBeenCalledWith(
      "did:example:123456789abcdefghi#key-1"
    );
  });
});
