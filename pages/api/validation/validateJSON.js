/**
 * Validate the given input as JSON
 * @function
 * @param {any} input - The input to be validated as JSON
 * @returns {boolean} - Returns true if the input is valid JSON, false otherwise
 */
export const validateJSON = (input) => {
  try {
    JSON.parse(input);
  } catch (e) {
    return false;
  }
  return true;
};
