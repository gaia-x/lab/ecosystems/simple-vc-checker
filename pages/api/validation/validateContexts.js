import validateJSON from "./validateJSON.js";

/**
 * Validate the contexts of the given document
 * @function
 * @param {Object} document - The document to validate the contexts of
 * @returns {boolean} - Returns true if all contexts in the document are valid, false otherwise
 */
export const validateContexts = (document) => {
  if (!document["@context"]) return false;

  const areContextsValid = document["@context"].map(async (context) => {
    if (typeof context !== "string") return false;
    try {
      const result = await (await fetch(context)).json();
      if (typeof result === "object") {
        const isValid = validateJSON(result);
        if (isValid) return true;
      } else return false;
    } catch (e) {
      return false;
    }
  });

  return areContextsValid.every((isValid) => isValid);
};
