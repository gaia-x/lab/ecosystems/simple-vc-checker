import { Ed25519VerificationKey2018 } from "@digitalbazaar/ed25519-verification-key-2018";
import { Ed25519VerificationKey2020 } from "@digitalbazaar/ed25519-verification-key-2020";
import jsonld from "jsonld";
import assert from "assert";
import { loadVerificationMethod } from "./loadVerificationMethod.js";

/**
 * check the signature of the given document
 * @function
 * @async
 * @param {Object} document - The document to check the signature of.
 * @returns {Promise<Boolean>} - Returns a promise that resolves to true if the signature is valid, false otherwise
 */

export const checkSignature = async (document) => {
  const options = {
    format: "application/n-quads",
    algorithm: "URDNA2015",
  };
  const proofs = Array.isArray(document.proof)
    ? document.proof
    : [document.proof];

  const canonized = await jsonld.canonize(document, options);

  let verified = true;

  for (let proof of proofs) {
    let verificationMethod = await loadVerificationMethod(
      proof.verificationMethod
    );
    let signature = null;

    if (proof.type === "Ed25519Signature2018") {
      verificationMethod = await Ed25519VerificationKey2018.from(
        verificationMethod
      );
      signature = proof.jws.split(".")[2];
      signature = Buffer.from(signature);
    } else if (proof.type === "Ed25519Signature2020") {
      verificationMethod = await Ed25519VerificationKey2020.from(
        verificationMethod
      );
      signature = Buffer.from(proof.proofValue);
    } else {
      assert.fail(`unknown verificationMethod ${proof.type}`);
    }

    const verifier = verificationMethod.verifier();
    const data = Buffer.from(canonized, "utf8");

    let response = await verifier.verify({ data, signature });

    console.log("response", response);

    verified = verified && response;
  }
  return verified;
};

