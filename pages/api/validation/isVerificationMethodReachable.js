import { loadVerificationMethod } from "./loadVerificationMethod.js";

/**
 * Check if the given document's verification method is reachable
 * @function
 * @async
 * @param {Object} document - The document to check the verification method of
 * @returns {Promise<boolean>} - Returns a promise that resolves to true if all verification methods in the document are reachable, false otherwise
 */
export const isVerificationMethodReachable = async (document) => {
  const proofs = Array.isArray(document.proof)
    ? document.proof
    : [document.proof];

  const result = proofs.map(async (proof) => {
    const verificationMethod = await loadVerificationMethod(
      proof.verificationMethod
    );
    return !!verificationMethod;
  });

  return result.every((isReachable) => isReachable);
};
