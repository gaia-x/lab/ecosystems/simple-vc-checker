import { validateContexts } from "./validateContexts.js";
import { validateJSON } from "./validateJSON.js";
import { checkSignature } from "./checkSignature.js";
import { isVerificationTypeSuppported } from "./isVerificationTypeSuppported.js";
import { isVerificationMethodReachable } from "./isVerificationMethodReachable.js";

/**
 * Validate the given verifiable credential
 * @function
 * @async
 * @param {Object} document - The verifiable credential to validate
 * @returns {Promise<Object>} - Returns a promise that resolves to an object containing the validation results for the given verifiable credential.
 */
export const validateVerifiableCredential = async (document) => {
  const response = {
    isValidJson: false,
    areContextsValid: false,
    isVerifiableCredential: false,
    isSignedVerifiableCredential: false,
    isSignatureTypeSupported: false,
    isVerificationMethodReachable: false,
    isValidSignature: false,
  };

  try {
    // Check JSON validity
    if (!validateJSON(JSON.stringify(document))) return response;
    response.isValidJson = true;

    // Check contexts' validity (reachable and valid JSON)
    if (validateContexts(document)) {
      response.areContextsValid = true;
    }

    // Check if it's a Verifiable Credential
    const types = document["type"];
    if (types && types.includes("VerifiableCredential")) {
      response.isVerifiableCredential = true;
    }

    // Check if VC is signed
    if (document["proof"]) {
      response.isSignedVerifiableCredential = true;
    }

    // Check if the signature type is supported
    if (isVerificationTypeSuppported(document)) {
      response.isSignatureTypeSupported = true;
    }

    // Check if the signature public key material is reachable
    if (await isVerificationMethodReachable(document)) {
      response.isVerificationMethodReachable = true;
    }

    // Check signature
    if (await checkSignature(document)) {
      response.isValidSignature = true;
    }

    return response;
  } catch (error) {
    console.log(error);
  }
};
