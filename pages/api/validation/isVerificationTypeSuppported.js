/**
 * Validate the contexts of the given document
 * @function
 * @param {Object} document - The document to validate the contexts of
 * @returns {boolean} - Returns a promise that resolves to true if all contexts in the document are valid, false otherwise
 */
export const isVerificationTypeSuppported = (document) => {
  const proofs = Array.isArray(document.proof)
    ? document.proof
    : [document.proof];

  const typeSupported = ["Ed25519Signature2018", "Ed25519Signature2020"];

  const result = proofs.map((proof) => {
    return typeSupported.includes(proof.type);
  });

  return result.every((isSupported) => isSupported);
};
