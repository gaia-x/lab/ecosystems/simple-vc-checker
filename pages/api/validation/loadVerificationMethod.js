import { driver } from "@digitalbazaar/did-method-key";
const didKeyDriver = driver();

/**
 * Loads a DID document from a given verification method.
 * @async
 * @param {string} verificationMethod - The method to be used for loading the document. Can be a URL starting with "https://" or a DID key starting with "did:key".
 * @returns {(Object|boolean)} Returns the document if successful, or false if an error occurs.
 */
export async function loadVerificationMethod(verificationMethod) {
  try {
    let didDocument = null;
    if (verificationMethod.startsWith("https://")) {
      didDocument = await fetch(verificationMethod);
      didDocument = await didDocument.json();
    } else if (verificationMethod.startsWith("did:key")) {
      didDocument = await didKeyDriver.get({ did: verificationMethod });
    } else return false;
    return didDocument;
  } catch (error) {
    return false;
  }
}
