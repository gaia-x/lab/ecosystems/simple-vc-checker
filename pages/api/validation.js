// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import { validateVerifiableCredential } from "pages/api/validation/validateVerifiableCredential";

export default async function handler(req, res) {
  if (req.method !== "POST") {
    res.status(405).send({ message: "Only POST requests allowed" });
    return;
  }
  if (!req.body) return res.status(400).send("No body found in request");

  let document;

  // If body is an url, fetch the document
  if (typeof req.body === "string" && req.body.startsWith("http")) {
    try {
      const response = await fetch(req.body);
      document = await response.json();
    } catch (error) {
      return res.status(400).send("Invalid URL");
    }
  } else {
    document = req.body;
  }

  const response = await validateVerifiableCredential(document);
  res.send(response);
}
