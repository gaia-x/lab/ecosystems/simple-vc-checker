import { Report } from "components/Report";
import {Box, Flex} from "@chakra-ui/react";
import { Header } from "components/Header";
import { useState } from "react";
import VCExemple from "../data/vc01.json";
import { JsonEditor } from "components/JsonEditor";
import Image from "next/image";
import dotsImg from "../public/dots.png";



export default function Home() {
  const [editorContent, setEditorContent] = useState(
    JSON.stringify(VCExemple, null, 2)
  );
  const [report, setReport] = useState({
    isValidJson: false,
    areContextsValid: false,
    isVerifiableCredential: false,
    isSignedVerifiableCredential: false,
    isSignatureTypeSupported: false,
    isVerificationMethodReachable: false,
    isValidSignature: false,
  });

  const [isLoading, setIsLoading] = useState(false);

  const callValidationApi = async (document) => {
    const response = await fetch("/api/validation", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: document,
    });
    return response.json();
  };

  const onSubmit = async () => {
    try {
      setIsLoading(true);
      const response = await callValidationApi(editorContent);
      if (response) {
        setReport(response);
      }
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      console.error(error);
    }
  };
  return (
    <Box bgColor={"#110094"} position={"relative"} h={"100vh"} overflow={"hidden"}>
      <Header />
      <Flex w={"100%"}>
        <Flex w={"50%"} justifyContent={"center"} alignItems={"flex-start"} mt={5} zIndex={2}>
          <JsonEditor
            value={editorContent}
            onChange={setEditorContent}
            onSubmit={onSubmit}
            isLoading={isLoading}
          />
        </Flex>
        <Flex
          w={"50%"}
          h={"90vh"}
          justifyContent={"center"}
          alignItems={"center"}
        >
          <Report report={report} />
        </Flex>
      </Flex>
      <Box position={"absolute"} top={'-10%'} left={"-0%"} zIndex={1}>
        <Image src={dotsImg}  />
      </Box>
      <Box position={"absolute"} top={'10%'} right={"-35%"} zIndex={1} transform={"rotate(270deg)"}>
        <Image src={dotsImg}  />
      </Box>
    </Box>
  );
}
