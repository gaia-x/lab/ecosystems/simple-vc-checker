import React from "react";
import { Center } from "@chakra-ui/react";

export const ReportCheckbox = ({ valid }) => {
  return <Center>{valid ? "✅" : "❌"}</Center>;
};
