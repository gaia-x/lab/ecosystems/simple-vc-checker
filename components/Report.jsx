import React, { useState } from "react";
import { Checkbox, Text, Flex, VStack } from "@chakra-ui/react";
import { ReportCheckbox } from "./ReportCheckbox";

export const Report = ({ report }) => {
  const designations = {
    isValidJson: "JSON is valid",
    areContextsValid: "Contexts are valid",
    isVerifiableCredential: "Is a Verifiable Credential",
    isSignedVerifiableCredential: "Is a signed Verifiable Credential",
    isSignatureTypeSupported: "Signature type is supported",
    isVerificationMethodReachable: "Verification method is reachable",
    isValidSignature: "Signature is valid",
  };

  return (
    <VStack align={"flex-start"} spacing={"50px"} color={"white"}>
      {Object.entries(report).map(([key, value]) => (
        <Flex key={key}>
          <ReportCheckbox valid={value} />
          <Text fontSize={"xl"} fontWeight={"bold"} ml={2}>{designations[key]}</Text>
        </Flex>
      ))}
    </VStack>
  );
};
