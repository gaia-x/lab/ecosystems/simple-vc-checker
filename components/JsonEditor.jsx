import React from "react";
import Editor from "@monaco-editor/react";
import { Flex, Button } from "@chakra-ui/react";

export const JsonEditor = ({ value, onChange, onSubmit, isLoading }) => {
  return (
    <Flex direction={"column"} w={"800px"}>
      <Flex h={"5vh"} w={"100%"} justifyContent={"flex-end"}>
        <Button
          colorScheme={"whiteAlpha"}
          onClick={onSubmit}
          isLoading={isLoading}
        >
          Submit
        </Button>
      </Flex>
      <Editor
        height="700px"
        defaultLanguage="json"
        value={value}
        onChange={(value) => (onChange ? onChange(value || "") : null)}
      />
    </Flex>
  );
};
