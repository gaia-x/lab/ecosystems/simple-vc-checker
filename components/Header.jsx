import React from "react";
import { Flex, Heading, Box } from "@chakra-ui/react";
import Image from "next/image";
import gxLogo from "../public/gx-logo-w.png";

export const Header = () => {
  return (
    <Flex alignItems={"center"} color={"white"} p={6}>
      <Box boxSize={"80px"}>
        <Image src={gxLogo} alt={"gaia-x-logo"} />
      </Box>
      <Heading zIndex={3} ml={3}>Gaia-X VC Checker</Heading>
    </Flex>
  );
};
